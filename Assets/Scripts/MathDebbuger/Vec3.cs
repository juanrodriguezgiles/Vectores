﻿using UnityEngine;
using System;
namespace CustomMath
{
    public struct Vec3 : IEquatable<Vec3>
    {
        #region Variables
        public float x;
        public float y;
        public float z;

        public float sqrMagnitude { get { return ((float)(Mathf.Pow(x, 2) + Mathf.Pow(y, 2) + Mathf.Pow(z, 2))); } }
        public Vec3 normalized => Vec3.Normalize(this);
        public float magnitude { get { return ((float)(Mathf.Sqrt(Mathf.Pow(x, 2) + Mathf.Pow(y, 2) + Mathf.Pow(z, 2)))); } }
        #endregion

        #region constants
        public const float epsilon = 1e-05f;
        #endregion

        #region Default Values
        public static Vec3 Zero { get { return new Vec3(0.0f, 0.0f, 0.0f); } }
        public static Vec3 One { get { return new Vec3(1.0f, 1.0f, 1.0f); } }
        public static Vec3 Forward { get { return new Vec3(0.0f, 0.0f, 1.0f); } }
        public static Vec3 Back { get { return new Vec3(0.0f, 0.0f, -1.0f); } }
        public static Vec3 Right { get { return new Vec3(1.0f, 0.0f, 0.0f); } }
        public static Vec3 Left { get { return new Vec3(-1.0f, 0.0f, 0.0f); } }
        public static Vec3 Up { get { return new Vec3(0.0f, 1.0f, 0.0f); } }
        public static Vec3 Down { get { return new Vec3(0.0f, -1.0f, 0.0f); } }
        public static Vec3 PositiveInfinity { get { return new Vec3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity); } }
        public static Vec3 NegativeInfinity { get { return new Vec3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity); } }
        #endregion                                                                                                                                                                               

        #region Constructors
        public Vec3(float x, float y)
        {
            this.x = x;
            this.y = y;
            this.z = 0.0f;
        }

        public Vec3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vec3(Vec3 v3)
        {
            this.x = v3.x;
            this.y = v3.y;
            this.z = v3.z;
        }

        public Vec3(Vector3 v3)
        {
            this.x = v3.x;
            this.y = v3.y;
            this.z = v3.z;
        }

        public Vec3(Vector2 v2)
        {
            this.x = v2.x;
            this.y = v2.y;
            this.z = 0.0f;
        }
        #endregion

        #region Operators
        //Es mas barato realizar la comparacion de esta manera
        public static bool operator ==(Vec3 left, Vec3 right)
        {
            float diff_x = left.x - right.x;
            float diff_y = left.y - right.y;
            float diff_z = left.z - right.z;
            float sqrmag = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
            return sqrmag < epsilon * epsilon;
        }
        public static bool operator !=(Vec3 left, Vec3 right)
        {
            return !(left == right);
        }

        public static Vec3 operator +(Vec3 leftV3, Vec3 rightV3)
        {
            return new Vec3(leftV3.x + rightV3.x, leftV3.y + rightV3.y, leftV3.z + rightV3.z);
        }

        public static Vec3 operator -(Vec3 leftV3, Vec3 rightV3)
        {
            return new Vec3(leftV3.x - rightV3.x, leftV3.y - rightV3.y, leftV3.z - rightV3.z);
        }

        public static Vec3 operator -(Vec3 v3)
        {
            return new Vec3(-v3.x, -v3.y, -v3.z);
        }

        public static Vec3 operator *(Vec3 v3, float scalar)
        {
            return new Vec3(v3.x * scalar, v3.y * scalar, v3.z * scalar);
        }
        public static Vec3 operator *(float scalar, Vec3 v3)
        {
            return new Vec3(v3.x * scalar, v3.y * scalar, v3.z * scalar);
        }
        public static Vec3 operator /(Vec3 v3, float scalar)
        {
            return new Vec3(v3.x / scalar, v3.y / scalar, v3.z / scalar);
        }

        public static implicit operator Vector3(Vec3 v3)
        {
            return new Vector3(v3.x, v3.y, v3.z);
        }

        public static implicit operator Vector2(Vec3 v2)
        {
            return new Vector2(v2.x, v2.y);
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "X = " + x.ToString() + "   Y = " + y.ToString() + "   Z = " + z.ToString();
        }
        public static float Angle(Vec3 from, Vec3 to)
        {
            float dot;
            float magnitudeFrom;
            float magnitudeTo;
            float angle;

            //Computo producto punto
            dot = Dot(from, to);
            //Obtengo magnitudes/distancias
            magnitudeFrom = Magnitude(from);
            magnitudeTo = Magnitude(to);
            //Divido el producto punto por las magnitudes
            angle = (dot / (magnitudeFrom * magnitudeTo));
            //Obtengo el angulo en base al arcoseno
            angle = Mathf.Acos(angle);
            return angle * Mathf.Rad2Deg;
        }
        public static Vec3 ClampMagnitude(Vec3 vector, float maxLength)
        {
            //Retorna un Vec3 con su longitud maxima en base a maxLenght
            //Si es menor lo retorna como esta y si es mayor lo clampea
            if (vector.magnitude > maxLength)
            {
                return vector.normalized * maxLength;
            }
            return vector;
        }
        public static float Magnitude(Vec3 vector)
        {
            //Obtengo la longitud del vector
            return ((float)(Mathf.Sqrt(Mathf.Pow(vector.x, 2) + Mathf.Pow(vector.y, 2) + Mathf.Pow(vector.z, 2))));
        }
        public static Vec3 Cross(Vec3 a, Vec3 b)
        {
            //El producto cruss retorna un tercer vector que es perpendicular a los 2 vectores proporcionados. La magnitud de este
            //es igual a las magnitued de los vectores a y b multiplicadas.
            return new Vec3((a.y * b.z - a.z * b.y), (a.z * b.x - a.x * b.z), (a.x * b.y - a.y * b.x));
        }
        public static float Distance(Vec3 a, Vec3 b)
        {
            return ((float)(Mathf.Sqrt(Mathf.Pow(a.x - b.x, 2) + Mathf.Pow(a.y - b.y, 2) + Mathf.Pow(a.z - b.z, 2))));
        }
        public static float Dot(Vec3 a, Vec3 b)
        {
            return (a.x * b.x + a.y * b.y + a.z * b.z);
        }
        public static Vec3 Lerp(Vec3 a, Vec3 b, float t)
        {
            return a + (b - a) * Mathf.Clamp01(t);
        }
        public static Vec3 LerpUnclamped(Vec3 a, Vec3 b, float t)
        {
            return (a + (b - a) * t);
        }
        public static Vec3 Max(Vec3 a, Vec3 b)
        {
            Vec3 c;

            c.x = (a.x > b.x) ? a.x : b.x;
            c.y = (a.y > b.y) ? a.y : b.y;
            c.z = (a.z > b.z) ? a.z : b.z;

            return c;
        }
        public static Vec3 Min(Vec3 a, Vec3 b)
        {
            Vec3 c;

            c.x = (a.x < b.x) ? a.x : b.x;
            c.y = (a.y < b.y) ? a.y : b.y;
            c.z = (a.z < b.z) ? a.z : b.z;

            return c;
        }
        public static float SqrMagnitude(Vec3 vector)
        {
            return ((float)(Mathf.Pow(vector.x, 2) + Mathf.Pow(vector.y, 2) + Mathf.Pow(vector.z, 2)));
        }
        public static Vec3 Project(Vec3 vector, Vec3 onNormal)
        {
            //"Reescala el vector onNormal para que su magnitud sea el punto mas cercano al otro vector"
            float dot = Dot(vector, onNormal);
            float magnitude = Magnitude(onNormal);

            return ((dot / Mathf.Pow(magnitude, 2)) * onNormal);
        }
        public static Vec3 Reflect(Vec3 inDirection, Vec3 inNormal)
        {
            //El vector inNormal define un plano y indirection es una flecha direccional con direccion al plano.
            //El vector resultante tiene la misma magnitud que inDirection pero con su direccion reflejada
            return (inDirection - 2 * Dot(inDirection, inNormal) * inNormal);
        }
        public void Set(float newX, float newY, float newZ)
        {
            x = newX;
            y = newY;
            z = newZ;
        }
        public void Scale(Vec3 scale)
        {
            x *= scale.x;
            y *= scale.y;
            z *= scale.z;
        }
        public static Vec3 Normalize(Vec3 vector)
        {
            float mag = Vec3.Magnitude(vector);
            return vector / mag;
        }
        #endregion

        #region Internals
        public override bool Equals(object other)
        {
            if (!(other is Vec3)) return false;
            return Equals((Vec3)other);
        }

        public bool Equals(Vec3 other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ (y.GetHashCode() << 2) ^ (z.GetHashCode() >> 2);
        }
        #endregion
    }
}