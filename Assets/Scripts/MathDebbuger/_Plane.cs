﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CustomMath;
namespace CustomMath
{
    public struct _Plane : IEquatable<_Plane>
    {
        #region VARIABLES
        private Vec3 inNormal;
        private float d;
        public float distance
        {
            get => this.d;
            set => this.d = value;
        }
        public _Plane flipped => new _Plane(-this.normal, -this.d);
        public Vec3 normal
        {
            get => this.inNormal;
            set => this.inNormal = value;
        }
        #endregion

        #region CONSTRUCTORS
        public _Plane(Vec3 inNormal, Vec3 inPoint)
        {
            this.inNormal = inNormal.normalized;
            this.d = Vec3.Dot(inNormal, inPoint);
        }
        public _Plane(Vec3 inNormal, float d)
        {
            this.inNormal = inNormal.normalized;
            this.d = d;
        }
        public _Plane(Vec3 a, Vec3 b, Vec3 c)
        {
            this.inNormal = Vec3.Cross(b - a, c - b).normalized;
            this.d = Vec3.Dot(this.inNormal, a);
        }
        #endregion

        #region FUNCTIONS
        public Vec3 ClosestPointOnPlane(Vec3 point)
        {
            float alfa = (this.d - Vec3.Dot(point, this.normal)) / Vec3.Dot(this.normal, this.normal);
            return point + alfa * this.normal;
        }
        public void Flip()
        {
            this.normal = -this.normal;
            this.d = -this.d;
        }
        public float GetDistanceToPoint(Vec3 point)
        {
            Vec3 closestPointVec3 = ClosestPointOnPlane(point);
            return Vec3.Distance(closestPointVec3, point);
        }
        public bool GetSide(Vec3 point)
        {
            return Vec3.Dot(this.normal, point) > this.d;
        }
        public bool SameSide(Vec3 inPt0, Vec3 inPt1)
        {
            return GetSide(inPt0) == GetSide(inPt1);
        }
        public void Set3Points(Vec3 a, Vec3 b, Vec3 c)
        {
            this.inNormal = Vec3.Cross(b - a, c - b).normalized;
            this.d = Vec3.Dot(this.inNormal, a);
        }
        public void SetNormalAndPosition(Vec3 inNormal, Vec3 inPoint)
        {
            this.inNormal = inNormal.normalized;
            this.d = Vec3.Dot(this.inNormal, inPoint);
        }
        public static _Plane Translate(_Plane plane, Vec3 translation)
        {
            Vec3 pointInPlaneVec3 = plane.ClosestPointOnPlane(Vec3.Zero);
            pointInPlaneVec3 += translation;
            return new _Plane(plane.normal, pointInPlaneVec3);
        }
        public void Translate(Vec3 translation)
        {
            Vec3 pointInPlaneVec3 = this.ClosestPointOnPlane(Vec3.Zero);
            pointInPlaneVec3 += translation;
            this.SetNormalAndPosition(this.inNormal, pointInPlaneVec3);
        }
        #endregion

        #region INTERNALS
        public override bool Equals(object other)
        {
            if (!(other is _Plane)) return false;
            return Equals((_Plane)other);
        }
        public bool Equals(_Plane other)
        {
            return inNormal == other.inNormal && d == other.d;
        }
        #endregion
    }
}