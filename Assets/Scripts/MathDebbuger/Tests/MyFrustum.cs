﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CustomMath;
using UnityEngine;
public class MyFrustum : MonoBehaviour
{
    #region INSTANCE
    private static MyFrustum instance;
    public static MyFrustum Get()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }
    #endregion
    public List<Transform> objectsInScene;

    FrustumCulling myFrustum;

    float nearDist = 5f;
    float farDist = 20f;
    float nearPlaneSize = 2;

    Vec3 currentPos;
    private float rotationX;
    private float rotationY;
    void Start()
    {
        currentPos = new Vec3(transform.position);
        rotationX = transform.eulerAngles.x * Mathf.Deg2Rad;
        rotationY = transform.eulerAngles.y * Mathf.Deg2Rad;

        myFrustum = new FrustumCulling(currentPos, nearDist, farDist, nearPlaneSize);
    }
    void Update()
    {
        if (transform.position != currentPos)
        {
            Vec3 offset = new Vec3(transform.position) - currentPos;
            currentPos = new Vec3(transform.position);

            myFrustum.Translate(offset);
            myFrustum.RenderGameObjects(objectsInScene);
        }
        if (transform.eulerAngles.x * Mathf.Deg2Rad != rotationX)
        {
            myFrustum.RotateX(rotationX - transform.eulerAngles.x * Mathf.Deg2Rad);
            rotationX = transform.eulerAngles.x * Mathf.Deg2Rad;
            myFrustum.RenderGameObjects(objectsInScene);
        }
        if (transform.eulerAngles.y * Mathf.Deg2Rad != rotationY)
        {
            myFrustum.RotateY(rotationY - transform.eulerAngles.y * Mathf.Deg2Rad);
            rotationY = transform.eulerAngles.y * Mathf.Deg2Rad;
            myFrustum.RenderGameObjects(objectsInScene);
        }

        myFrustum.DrawFrustum(new Vec3(transform.position), nearDist, farDist,nearPlaneSize);
    }
}