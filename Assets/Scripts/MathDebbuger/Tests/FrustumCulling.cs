﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using CustomMath;
public struct FrustumCulling
{
    public List<_Plane> planes;
    #region CONSTRUCTOR
    public FrustumCulling(Vec3 position, float nearDist, float farDist, float squareSize)
    {
        planes = new List<_Plane>();
        //0NEAR---------------------------------------------------------------------------------------------
        Vec3 nearTopL = new Vec3(position.x - squareSize, position.y + squareSize, position.z + nearDist);
        Vec3 nearTopR = new Vec3(position.x + squareSize, position.y + squareSize, position.z + nearDist);
        Vec3 nearBotL = new Vec3(position.x - squareSize, position.y - squareSize, position.z + nearDist);
        Vec3 nearBotR = new Vec3(position.x + squareSize, position.y - squareSize, position.z + nearDist);

        _Plane nearPlane = new _Plane(Vec3.Forward,
            new Vec3(position.x, position.y, position.z + nearDist));
        planes.Add(nearPlane);
        //1FAR-------------------------------------------------------------------------------------------------
        _Plane farPlane = new _Plane(-Vec3.Forward,
            new Vec3(position.x, position.y, position.z + farDist));
        planes.Add(farPlane);
        //2TOP-----------------------------------------------------------------------------------------------
        _Plane topPlane = new _Plane(Vec3.Cross(nearTopR, nearTopL), new Vec3(position));
        planes.Add(topPlane);
        //3BOT-----------------------------------------------------------------------------------------------
        _Plane bottomPlane = new _Plane(Vec3.Cross(nearBotL, nearBotR), new Vec3(position));
        planes.Add(bottomPlane);
        //4LEFT---------------------------------------------------------------------------------------------
        _Plane leftPlane = new _Plane(Vec3.Cross(nearTopL, nearBotL), new Vec3(position));
        planes.Add(leftPlane);
        //5RIGHT-------------------------------------------------------------------------------------------
        _Plane rightPlane = new _Plane(Vec3.Cross(nearBotR, nearTopR), new Vec3(position));
        planes.Add(rightPlane);
    }
    #endregion
    #region FUNCTIONS
    //--------------------------------------------------------------------------------
    public void RenderGameObjects(List<Transform> objectsInScene)
    {
        foreach (Transform objTransform in objectsInScene)
        {
            Mesh objMesh = objTransform.GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = objMesh.vertices;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = objTransform.TransformPoint(vertices[i]);
            }

            objTransform.GetComponent<MeshRenderer>().enabled = IsObjectInFrustum(vertices);
        }
    }
    bool IsObjectInFrustum(Vector3[] vertices)
    {
        foreach (Vector3 vertexVector3 in vertices)
        {
            Vec3 vertexVec3 = new Vec3(vertexVector3);

            if (planes[0].GetSide(vertexVec3) && planes[1].GetSide(vertexVec3) && planes[2].GetSide(vertexVec3) &&
                planes[3].GetSide(vertexVec3) && planes[4].GetSide(vertexVec3) && planes[5].GetSide(vertexVec3))
                return true;
        }
        return false;
    }
    //--------------------------------------------------------------------------------
    public void Translate(Vec3 offset)
    {
        for (int i = 0; i < planes.Count; i++)
        {
            planes[i] = _Plane.Translate(planes[i], offset);
        }
    }
    //--------------------------------------------------------------------------------
    public void RotateX(float angle)
    {
        for (int i = 0; i < planes.Count; i++)
        {
            _Plane currentPlane = this.planes[i];
            float x = currentPlane.normal.x;
            float y = currentPlane.normal.y * Mathf.Cos(angle) - currentPlane.normal.z * Mathf.Sign(angle);
            float z = currentPlane.normal.y * Mathf.Sin(angle) + currentPlane.normal.z * Mathf.Cos(angle);
            this.planes[i] = new _Plane(new Vec3(x, y, z), currentPlane.distance);
        }
    }
    //--------------------------------------------------------------------------------
    public void RotateY(float angle)
    {
        for (int i = 0; i < planes.Count; i++)
        {
            _Plane currentPlane = this.planes[i];
            float x = currentPlane.normal.x * Mathf.Cos(angle) + currentPlane.normal.z * Mathf.Sin(angle);
            float y = currentPlane.normal.y;
            float z = -currentPlane.normal.x * Mathf.Sin(angle) + currentPlane.normal.z * Mathf.Cos(angle);
            this.planes[i] = new _Plane(new Vec3(x, y, z), currentPlane.distance);
        }
    }
    //--------------------------------------------------------------------------------
    public void DrawFrustum(Vec3 frustumPos, float nearDist, float farDist, float squareSize)
    {
        Vec3 farTopL = new Vec3(frustumPos) + new Vec3(-squareSize * farDist / nearDist, squareSize * farDist / nearDist, farDist);
        Vec3 farTopR = new Vec3(frustumPos) + new Vec3(squareSize * farDist / nearDist, squareSize * farDist / nearDist, farDist);
        Vec3 farBotL = new Vec3(frustumPos) + new Vec3(-squareSize * farDist / nearDist, -squareSize * farDist / nearDist, farDist);
        Vec3 farBotR = new Vec3(frustumPos) + new Vec3(squareSize * farDist / nearDist, -squareSize * farDist / nearDist, farDist);

        Vec3 nearTopL = new Vec3(frustumPos.x - squareSize, frustumPos.y + squareSize, frustumPos.z + nearDist);
        Vec3 nearTopR = new Vec3(frustumPos.x + squareSize, frustumPos.y + squareSize, frustumPos.z + nearDist);
        Vec3 nearBotL = new Vec3(frustumPos.x - squareSize, frustumPos.y - squareSize, frustumPos.z + nearDist);
        Vec3 nearBotR = new Vec3(frustumPos.x + squareSize, frustumPos.y - squareSize, frustumPos.z + nearDist);

        Debug.DrawLine(farTopL, farTopR);
        Debug.DrawLine(farTopR, farBotR);
        Debug.DrawLine(farBotR, farBotL);
        Debug.DrawLine(farBotL, farTopL);

        Debug.DrawLine(nearTopL, nearTopR);
        Debug.DrawLine(nearTopR, nearBotR);
        Debug.DrawLine(nearBotR, nearBotL);
        Debug.DrawLine(nearBotL, nearTopL);

        Debug.DrawRay(frustumPos, new Vector3(-squareSize, squareSize, nearDist) * farDist / nearDist);
        Debug.DrawRay(frustumPos, new Vector3(squareSize, squareSize, nearDist) * farDist / nearDist);
        Debug.DrawRay(frustumPos, new Vector3(-squareSize, -squareSize, nearDist) * farDist / nearDist);
        Debug.DrawRay(frustumPos, new Vector3(squareSize, -squareSize, nearDist) * farDist / nearDist);
    }
    #endregion
}