﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using CustomMath;
public class BoxExample : MonoBehaviour
{
    public GameObject cuwu;
    private Vec3 p1Vec3 = new Vec3(0, 0, 0);
    private Vec3 p2Vec3 = new Vec3(0, 0, 10);
    private Vec3 p3Vec3 = new Vec3(-5, 0, 5);
    private Vec3 p4Vec3 = new Vec3(5, 0, 5);
    private Vec3 p5Vec3 = new Vec3(0, 0, 5);
    private Vec3 p6Vec3 = new Vec3(0, 5, 5);
    private _Plane plane1;
    private _Plane plane2;
    private _Plane plane3;
    private _Plane plane4;
    private _Plane plane5;
    private _Plane plane6;
    public Vector3[] verticesReal = new Vector3[8];
    void Start()
    {
        plane1 = new _Plane(new Vec3(0, 0, 1), p1Vec3); //1
        plane2 = new _Plane(new Vec3(0, 0, -1), p2Vec3);//2
        plane3 = new _Plane(new Vec3(1, 0, 0), p3Vec3);//3
        plane4 = new _Plane(new Vec3(-1, 0, 0), p4Vec3);//4
        plane5 = new _Plane(new Vec3(0, 1, 0), p5Vec3);//5
        plane6 = new _Plane(new Vec3(0, -1, 0), p6Vec3);//6
    }
    void Update()
    {
        Mesh cubeMesh = cuwu.GetComponent<MeshFilter>().mesh;
        verticesReal = cubeMesh.vertices;
        for (int i = 0; i < verticesReal.Length; i++)
        {
            verticesReal[i] = transform.TransformPoint(verticesReal[i]);
        }
        checkVertices();
    }
    void checkVertices()
    {
        Vec3 vertex;
        for (int i = 0; i < verticesReal.Length; i++)
        {
            vertex = new Vec3(verticesReal[i].x, verticesReal[i].y, verticesReal[i].z);
            if (!plane1.GetSide(vertex) || !plane2.GetSide(vertex) || !plane3.GetSide(vertex) ||
                !plane4.GetSide(vertex) || !plane5.GetSide(vertex) || !plane6.GetSide(vertex)) 
            {
                Debug.Log("No");
                return;
            }
        }
        Debug.Log("Yes");
    }
}