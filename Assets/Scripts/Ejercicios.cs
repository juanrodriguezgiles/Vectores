﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;
namespace EjerciciosAlgebra
{
    public class Ejercicios : MonoBehaviour
    {
        public enum ej
        {
            Uno = 1,
            Dos,
            Tres,
            Cuatro,
            Cinco,
            Seis,
            Siete,
            Ocho,
            Nueve,
            Diez
        };
        public ej ejercicio = ej.Uno;
        public Color VectorColor = Color.red;
        public Vec3 A = new Vec3(1, 2, 3);
        public Vec3 B = new Vec3(4, 5, 6);
        private Vec3 C = new Vec3(0, 0, 0);
        float time = 0;
        // Start is called before the first frame update
        void Start()
        {
            VectorDebugger.EnableCoordinates();
            VectorDebugger.EnableEditorView();
            VectorDebugger.AddVector(new Vec3(0, 0, 0), "A");
            VectorDebugger.AddVector(new Vec3(0, 0, 0), "B");
            VectorDebugger.AddVector(new Vec3(0, 0, 0), "C");
        }

        // Update is called once per frame
        void Update()
        {
            switch (ejercicio)
            {
                case ej.Uno:
                    C = A + B;
                    break;
                case ej.Dos:
                    C = A - B;
                    break;
                case ej.Tres:
                    C.x = A.x * B.x;
                    C.y = A.y * B.y;
                    C.z = A.z * B.z;
                    break;
                case ej.Cuatro:
                    C = Vec3.Cross(B, A);
                    break;
                case ej.Cinco:
                    time += Time.deltaTime;
                    C = Vec3.Lerp(A, B, time);
                    if (time > 1)
                        time = 0;
                    break;
                case ej.Seis:
                    C = Vec3.Max(A, B);
                    break;
                case ej.Siete:
                    C = Vec3.Project(A, B);
                    break;
                case ej.Ocho://??
                    break;
                case ej.Nueve:
                    C = Vec3.Reflect(A, B.normalized);
                    break;
                case ej.Diez:
                    time += Time.deltaTime;
                    C = Vec3.LerpUnclamped(B, A, time);
                    if (time > 10)
                        time = 0;
                    break;
            }
            VectorDebugger.UpdateColor("A", Color.white);
            VectorDebugger.UpdateColor("B", Color.black);
            VectorDebugger.UpdateColor("C", VectorColor);
            VectorDebugger.UpdatePosition("A", A);
            VectorDebugger.UpdatePosition("B", B);
            VectorDebugger.UpdatePosition("C", C);
            Debug.Log(C);
        }
    }
}

